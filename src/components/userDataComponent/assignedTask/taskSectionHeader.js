import React from 'react'
import '../style.css'

function TaskSectionHeader(props) {
    const { userTaskGroupList, selectedGroup, onSelectGroup, setVisibleBody, onDeleteGroup, isFromChat, isFromNote, isAboveItem } = props;
    console.log(" _____________ props ", props)
    
    const onHandleClick = (item) => {
        onSelectGroup(item.id);
        setVisibleBody(true);
    }
    const onClickAddNewTask = () => {
        onSelectGroup("");
        setVisibleBody(true);
    }
    const onClickDelete = (item) => {
        onDeleteGroup(item.id);
    }
    const userTaskGroupUIList = userTaskGroupList.map((item) => {
        return (
            <div key={`user_chat_task_key_${item.id}`}>
                {isFromChat == true ?
                    <div style={{ textAlign: 'center' }}>
                        <div className={item.is_template === "0" ? "patient-page-existing-task-style" : "patient-page-existing-task-title-style-complete"}
                            style={{ width: "95%" }}>
                            <div style={{ width: "100%" }}>
                                <span className="float-left" style={item.is_template === "0" ? { color: 'black' } : { color: 'white' }}
                                    onClick={() => onHandleClick(item)}>
                                    {item.group_name}
                                </span>
                                {/* <span className="float-right" style={item.is_template === "0" ? { color: 'darkgray' } : { color: 'white' }}>
                                    {item.is_template}
                                </span> */}
                                {/* <span className="patient-page-delete-group-button float-right"
                                    style={{ marginRight: '5%' }}
                                    onClick={() => onClickDelete(item)}
                                >
                                    &times;
                                </span> */}
                            </div>
                        </div>
                    </div>
                    : <div className={isFromNote ? "patient-page-existing-task-style-from-note" : "patient-page-existing-task-style"}>
                        <div className="patient-page-existing-task-title-style" >
                            <span onClick={() => onHandleClick(item)}>
                                {item.group_name}
                            </span>
                            <span className="patient-page-delete-group-button"
                                onClick={() => onClickDelete(item)}>
                                &times;
                        </span>
                        </div>
                    </div>}
            </div>
        );
    });

    return (
        <div>
            {isFromChat == true
                ? <div>
                    {userTaskGroupUIList}
                    <div
                        // className="patient-page-existing-task-completed-style add-button patient-page-add-task-button-style"
                        className="patient-page-add-task-button-style"
                        style={{ width: "95%", textAlign: 'center' }}
                        onClick={onClickAddNewTask}>
                        Add New Task
                    </div>
                </div>
                :
                isFromNote == true
                    ? isAboveItem == true
                        ? <div className="patient-page-existing-task-container-style">
                            {userTaskGroupUIList}
                        </div>
                        : <div className="patient-page-existing-task-container-style">
                            <div
                                // className="patient-page-existing-task-completed-style add-button patient-page-add-task-button-style"
                                className="patient-page-add-task-button-style"
                                onClick={onClickAddNewTask}>
                                Add New Task
                            </div>
                        </div>
                    :
                    <div className="patient-page-existing-task-container-style">
                        <div
                            // className="patient-page-existing-task-completed-style add-button patient-page-add-task-button-style"
                            className="patient-page-add-task-button-style"
                            onClick={onClickAddNewTask}>
                            Add New Task
                        </div>
                        {userTaskGroupUIList}
                        <div className="assigned-tasks-container row"></div>
                    </div>

            }

        </div>
    )
}

export default TaskSectionHeader
